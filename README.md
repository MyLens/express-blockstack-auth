# Express-Blockstack-auth

Middleware for validating a blockstack user

## Install 
`npm install @mylens/express-blockstack-auth`

## Usage with express: 
You need to pass a list of dapps that can this user can be authenticated with. 

```js
app.get('/', Auth(['https://app.mylens.io', 'https://localhost:*']), (req,res) => {
  if (req.user === "lensadmin.id.blockstack") {
   // Do admin stuff
  } else {
  // Do regular user stuff. 
  }
})

```
