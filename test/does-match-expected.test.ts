
import {doesMatchExpected} from '../lib/verify-token'


test('Test doesMatchExpected conditions', async () => {

    expect(doesMatchExpected("http://localhost:3000", [
        "http://localhost:3000"
    ])).toBe(true);
    expect(doesMatchExpected("http://localhost:3000", [
        "https://bad",
        "http://localhost:3000"
    ])).toBe(true);
    
    expect(doesMatchExpected("http://localhost:3000", [
        "https://bad",
        "http://nothing"
    ])).toBe(false);


    expect(doesMatchExpected("http://localhost:3000", [
        "http://*:3000"
    ])).toBe(true);
    expect(doesMatchExpected("http://localhost:3000", [
        "http://*:3000",
        "http://localhost*"
    ])).toBe(true);
    
    expect(doesMatchExpected("http://localhost:3000", [
        "http://*:*"
    ])).toBe(true);
    
    expect(doesMatchExpected("http://localhost:3000", [
        "http://*:*bad"
    ])).toBe(false);

    
    expect(doesMatchExpected("http://localhost:3000", [
        "http://localhost:3000",

    ])).toBe(true);

    
    expect(doesMatchExpected("https://deploy-preview-25--stoic-pare-596843.netlify.com", [
        "http://localhost:3000",
        "https://app.mylens.io",
        "https://deploy-preview-*--stoic-pare-596843.netlify.com"
    ])).toBe(true);
})