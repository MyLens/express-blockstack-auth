// const test = require('tape');
// const blockstack = require('blockstack')
// const { verifyAPIAuthToken, generateAPIAuthToken } = require('../verify-api-auth-token')

import {generateAPIAuthToken} from '../lib/generate-token'
import {verifyAPIAuthToken} from '../lib/verify-token'
import { UserDataInterface } from '../lib/interfaces/user-data';

const expectedApps = ['https://app.mylens.io']

test('Authorization passes', async () => {
    let userData : UserDataInterface = {
        username: process.env.BLOCKSTACK_ID || "", 
        gaiaHubConfig: {
            address: process.env.GAIAHUB_CONFIG_ADDRESS || ""
        }, 
        appPrivateKey: process.env.APP_PRIVATE_KEY || ""
    }

    let auth = generateAPIAuthToken(userData)
    let id = await verifyAPIAuthToken(auth, expectedApps)
    expect(id).toBe(userData.username)
})

test('Authorization fails for non-existent id', async () => {
    let userData = {
        username: "bad-user.id.blockstack", 
        gaiaHubConfig: {
            address: process.env.GAIAHUB_CONFIG_ADDRESS || ""
        }, 
        appPrivateKey: process.env.APP_PRIVATE_KEY || ""
    }

    let auth = generateAPIAuthToken(userData)
    await expect(verifyAPIAuthToken(auth, expectedApps)).rejects.toThrow('Name not found')
})

test('Authorization fails for mismatched user.', async () => {
    let userData = {
        username: "emptywells.id.blockstack", 
        gaiaHubConfig: {
            address: process.env.GAIAHUB_CONFIG_ADDRESS || ""
        }, 
        appPrivateKey: process.env.APP_PRIVATE_KEY || ""
    }

    let auth = generateAPIAuthToken(userData)
    await expect(verifyAPIAuthToken(auth, expectedApps)).rejects.toThrow()
})