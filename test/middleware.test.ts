import { UserDataInterface } from "../lib/interfaces/user-data";
import { generateAPIAuthToken } from "../lib/generate-token";
import { Auth } from '../lib/index'
import {UnauthorizedError} from '../lib/errors/unauthorized-error'

const expectedApps = ['https://app.mylens.io']


test('Passing token: ', async () => {
    let userData : UserDataInterface = {
        username: process.env.BLOCKSTACK_ID || "", 
        gaiaHubConfig: {
            address: process.env.GAIAHUB_CONFIG_ADDRESS || ""
        }, 
        appPrivateKey: process.env.APP_PRIVATE_KEY || ""
    }
    let auth = generateAPIAuthToken(userData)
    let req :any = {
        headers: {
            authorization: `Bearer ${auth}`
        }
    }
    let res = {}
    

    await Auth(expectedApps)(req, res, e => {
        expect(e).toBe(undefined)
        expect(req.user).toBe(userData.username)
    })

})

test('Bad authorization: ', async () => {
    let userData : UserDataInterface = {
        username: process.env.BLOCKSTACK_ID || "", 
        gaiaHubConfig: {
            address: process.env.GAIAHUB_CONFIG_ADDRESS || ""
        }, 
        appPrivateKey: process.env.APP_PRIVATE_KEY || ""
    }
    let auth = generateAPIAuthToken(userData)
    let req :any = {
        headers: {
            authorization: `${auth}`
        }
    }
    let res = {}
    

    await Auth(expectedApps)(req, res, e => {
        expect(e).toBeInstanceOf(UnauthorizedError)
        expect(e.status).toBe(401)
        expect(req.user).toBe(undefined)
    })

})