import * as express from "express";
import { CacheInterface } from "./interfaces/cache";
import { verifyAPIAuthToken } from "./verify-token";
import { generateAPIAuthToken } from "./generate-token"
import {UnauthorizedError} from './errors/unauthorized-error'

// type MiddlewareFunctionType = (req: any, res: any, next: express.NextFunction) => Promise<void>

/**
 * Middeleware for validating a blockstack user with express. If authentication passes, a user
 * object is attached to req, `req.user` which can then be used for authorization. 
 * @param expectedApps - These are the blockstack apps we are looking to use for verification. For example, 
 * if you want to verify john.id.blockstack on mylens, you would simply need to set the expected apps array 
 * to `['https://app.mylens.io']`. Wildcards can also be used such as `["http://local*"]` to match "http://localhost:3000".
 * @param userToPublicKeyCache - If a caching mechanism is desired so that you don't have to search for the 
 * blockstack gaia address eachtime, you can optionally pass in a caching mechanism here
 */
function Auth(expectedApps: string[], userToPublicKeyCache?: CacheInterface): any {

    var middleware : any = async function (req: any, res: any, next: express.NextFunction): Promise<void> {
        if (req.headers && req.headers.authorization) {
            var parts = req.headers.authorization.split(' ');
            if (parts.length == 2) {
                var scheme = parts[0];
                var credentials = parts[1];

                if (/^Bearer$/i.test(scheme)) {

                    try {
                        const id = await verifyAPIAuthToken(credentials, expectedApps, userToPublicKeyCache)
                        req.user = id
                        next()
                    } catch (e) {
                        next(new UnauthorizedError("bad_auth_token","Bad authorization token: " + e))
                    }
                } else {
                    return next(new UnauthorizedError("format_failure", "format is: Authorization: Bearer [token]"))
                }
            } else {
                return next(new UnauthorizedError("format_failure", "format is: Authorization: Bearer [token]"))
            }
        } else {
            return next(new UnauthorizedError("missing_header", "Missing authorization header, format is Authorization: Bearer [token]"))
        }
    }
    middleware.UnauthorizedError = UnauthorizedError;
    return middleware
}

export {Auth, CacheInterface, generateAPIAuthToken}
