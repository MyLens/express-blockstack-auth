export class UnauthorizedError extends Error {
    
    code : string; 
    status: number; 
    inner: string
    constructor(code: string, error: string){
        super(error)
        this.code = code 
        this.status = 401; 
        this.inner = error
    }
}