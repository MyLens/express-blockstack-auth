import * as blockstack from 'blockstack'
import { UserDataInterface } from './interfaces/user-data';

export function generateAPIAuthToken(userData: UserDataInterface) {
    const data = {
        id: userData.username,
        publicKey: userData.gaiaHubConfig.address
    }
    return blockstack.signProfileToken(data, userData.appPrivateKey);
}
