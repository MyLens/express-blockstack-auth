export interface UserDataInterface {
    username: string, 
    appPrivateKey: string, 
    gaiaHubConfig: {
        address: string
    }
}
