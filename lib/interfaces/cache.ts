export interface CacheInterface {
    get(id: string, publicKey: string) : boolean
    put(id: string, publicKey: string, publicKeyIsForUserId: boolean) : void
}
