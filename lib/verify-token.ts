import * as blockstack from 'blockstack'
import { CacheInterface } from './interfaces/cache';


export function doesMatchExpected(app: string, expectedApps: Array<string>) : boolean {
    for(let x=0;x<expectedApps.length;x++) {
        const eApp = expectedApps[x];
        // Create a regex out of it.
        const rx = new RegExp("^"+eApp
            .split("*")
            .map(p => p.replace(/[.?+^$[\]\\(){}|-]/g, "\\$&"))
            .join(".*")+"$");
     
        // If the app matches, we're good.
        if(rx.test(app)) return true;
    }        
    // Nothing matched.
    return false;
}

export async function verifyAPIAuthToken(token : string, expectedApps: string[], userToPublicKeyCache?: CacheInterface) {
    const jToken = blockstack.decodeToken(token);
    
    // Token must have a publicKey in claim.
    const payload = jToken.payload as { [key:string]: any };
    if(!payload || !payload.claim || !payload.claim.publicKey) {
        throw new Error("Token must have a publicKey in the claim.")
    }
    const publicKey = payload.claim.publicKey;
    const id = payload.claim.id;

    const tokenProfile = blockstack.verifyProfileToken(token, publicKey);

    // token must be valid + signed.
    if(!tokenProfile) {
        throw new Error("Invalid profile token provided. ")
    }

    let publicKeyIsForUserId = false;
    let cacheMiss = true;
    // This part is expensive, so check the cache.
    if(userToPublicKeyCache){
        const cacheEntry = await userToPublicKeyCache.get(id, publicKey);
        cacheMiss = false;

        publicKeyIsForUserId = cacheEntry;
    } 
    
    if(cacheMiss) {

        // Token's publicKey and Id must match up to the expected app's
        const profile = await blockstack.lookupProfile(id);

        publicKeyIsForUserId = Object.keys(profile.apps)
            // Get an array of the profile apps.
            .map(key => { return { host: key, gaia: profile.apps[key] }; })
            // Only keep those that match one of our expected apps.
            .filter(app => doesMatchExpected(app.host, expectedApps))
            // Get the index (if any) of the entry who's gaia contains our public key
            .findIndex(app => app.gaia.indexOf(publicKey) != -1) 
            // As long as we have a value that isn't -1, we found something.
            != -1;

        if(userToPublicKeyCache) {
            await userToPublicKeyCache.put(id, publicKey, publicKeyIsForUserId);
        }
    }
    if(!publicKeyIsForUserId) throw new Error(`ID "${id}" is not associated with public key "${publicKey}" for expected apps: ${expectedApps}`);
    
    return id;
}
